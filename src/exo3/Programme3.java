package exo3;

import java.util.Scanner;

public class Programme3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
// exo1 : Programme qui demande deux chiffres � l'utilisateur, les divise et affiche le resultat

		System.out.println("Merci de saisir le 1er chiffre");
		Scanner scanner = new Scanner(System.in);
		int prem = scanner.nextInt();

		System.out.println("Merci de saisir le 2nd chiffre (non nul)");
		int sec = scanner.nextInt();
		if (sec == 0) { 
			System.out.println("On a dit pas nul");
		}
		float resdiv = prem / sec;
		System.out.println("la division du premier chiffre par le second donne " + resdiv);
		
		float resmult = prem * sec;
		System.out.println("la multiplication du premier chiffre par le second donne " + resmult);
		
		float ressous = prem - sec;
		System.out.println("la soustraction du premier chiffre par le second donne " + ressous);
		
		float ressom = prem + sec;
		System.out.println("l'addition du premier chiffre par le second donne " + ressom);
		
		float resrest = prem % sec;
		System.out.println("le reste du premier chiffre divise par le second donne " + resrest);
	}

}
